from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from django.forms import model_to_dict
from .models import Note
from .serializers import NoteSerializer

class NoteViewSet(viewsets.ModelViewSet):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer
    permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        user = self.request.user
        queryset = self.get_queryset()
        queryset = queryset.filter(user=user, is_deleted=False)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=['delete'], detail=True)
    def delete(self, request, pk=None):
        ''' Action to delete note'''
        note = self.get_object()
        note.is_deleted = True
        note.save()

        data = model_to_dict(note, fields=('id', 'is_deleted'))
        headers = self.get_success_headers(data)
        return Response(data, status=status.HTTP_204_NO_CONTENT, headers=headers)

    @action(methods=['get'], detail=False)
    def deleted(self, request):
        ''' return list of deleted notes'''
        user = self.request.user
        deleted_notes = Note.objects.filter(user=user, is_deleted=True)

        serializer = self.get_serializer(deleted_notes, many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def restore(self, request, pk=None):
        ''' action to restore notes'''
        note = self.get_object()
        note.is_deleted = False
        note.save()

        data = {'is_deleted': False}
        headers = self.get_success_headers(data)
        return Response(data, status=status.HTTP_204_NO_CONTENT, headers=headers)
