from rest_framework import routers

from .views import NoteViewSet


urlpatterns = []
router = routers.DefaultRouter()

router.register(r'note', NoteViewSet)



urlpatterns += router.urls
