from django.contrib import admin

from .models import Note, Checklist


class ChecklistInline(admin.TabularInline):
    model = Checklist
    extra = 1


class NoteAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')
    inlines =[ChecklistInline]


admin.site.register(Note, NoteAdmin)
