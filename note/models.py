from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()

class Base(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Note(Base):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.TextField()
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.title


class Checklist(Base):
    note = models.ForeignKey(Note, related_name="checklists", on_delete=models.CASCADE)
    item = models.CharField(max_length=255)
    is_complete = models.BooleanField(default=False)

    def __str(self):
        return self.item